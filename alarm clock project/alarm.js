$("button#setButton").click(function () {

	var weatherAPI = "http://api.apixu.com/v1/forecast.json?key=94c03e142c844358ac7104457171907";
	$.getJSON(weatherAPI, {
			q: "bs65sq",
			days: 2,
			format: "json"
		})
		.done(function (input) {

			var output = [];

			var output2 = [];

			var hr = document.getElementById('alarmhrs');
			var selectedHour = hr.options[hr.selectedIndex].value;
			var weatherArrayCurrent = [input.forecast.forecastday[0].hour];
			var weatherArrayNextDay = [input.forecast.forecastday[1].hour];

			// var todayTomorrow = weatherArrayCurrent.concat(weatherArrayNextDay)

			// console.log(todayTomorrow);

			// starts loop from the hour alarm is set to go off
			for (var i = selectedHour; i < input.forecast.forecastday[0].hour.length; i++)
				output.push(input.forecast.forecastday[0].hour[i].temp_c);

			for (var j = 0 ; j < (24 - output.length); j++)
				output2.push(input.forecast.forecastday[1].hour[j].temp_c);

			var twentyFourHours = output.concat(output2);

			var s = Smooth(twentyFourHours);
			console.log(s);

			console.log("24hrs : " + twentyFourHours);
		
			playAlarm(twentyFourHours);


			
			function playAlarm(notes) {
				if (!window.AudioContext && !window.webkitAudioContext) {
					return;
				}
				var playing = null;
				var note = 0;
				var output = new (window.AudioContext || window.webkitAudioContext)();
				var instrument = output.createOscillator();
				var amplifier = output.createGain();
				var playNotes = function() {
					if (note < notes.length) {
						instrument.frequency.value = 440 + (notes[note] * 64); // hertz
						note = note + 1;
					} else {
						amplifier.gain.value = 0;
					}
					playing = window.setTimeout(playNotes, 250);
				};
				instrument.type = 'triangle'; // 'sine', 'square', 'sawtooth', 'triangle'
				instrument.start();
				instrument.connect(amplifier);
				amplifier.gain.value = 0.7;
				amplifier.connect(output.destination);
				playNotes();
			}
		});

});


var sound = new Audio("https://www.freespecialeffects.co.uk/soundfx/animals/duck1.wav");
sound.loop = true;

var h2 = document.getElementById('clock');

// display current time by the second
var currentTime = setInterval(function () {
	var date = new Date();

	var hours = date.getHours();
	// var hours = date.getHours();

	var minutes = date.getMinutes();

	var seconds = date.getSeconds();


	h2.textContent = addZero(hours) + ":" + addZero(minutes) + ":" + addZero(seconds)

}, 1000);


/*functions to get hour, min, secs, 
  am or pm, add zero, set alarm time and sound, clear alarm
*/

function addZero(time) {

	return (time < 10) ? "0" + time : time;

}

function hoursMenu() {

	var select = document.getElementById('alarmhrs');
	var hrs = 24

	for (i = 1; i <= hrs; i++) {
		select.options[select.options.length] = new Option(i < 10 ? "0" + i : i, i);

	}
}
hoursMenu();

function minMenu() {

	var select = document.getElementById('alarmmins');
	var min = 59;

	for (i = 0; i <= min; i++) {
		select.options[select.options.length] = new Option(i < 10 ? "0" + i : i, i);
	}
}
minMenu();

function secMenu() {

	var select = document.getElementById('alarmsecs');
	var sec = 59;

	for (i = 0; i <= sec; i++) {
		select.options[select.options.length] = new Option(i < 10 ? "0" + i : i, i);
	}
}
secMenu();


function alarmSet() {

	var hr = document.getElementById('alarmhrs');

	var min = document.getElementById('alarmmins');

	var sec = document.getElementById('alarmsecs');



	var selectedHour = hr.options[hr.selectedIndex].value;
	var selectedMin = min.options[min.selectedIndex].value;
	var selectedSec = sec.options[sec.selectedIndex].value;

	var alarmTime = addZero(selectedHour) + ":" + addZero(selectedMin) + ":" + addZero(selectedSec)
	console.log('alarmTime:' + alarmTime);

	document.getElementById('alarmhrs').disabled = true;
	document.getElementById('alarmmins').disabled = true;
	document.getElementById('alarmsecs').disabled = true;


	//when alarmtime is equal to currenttime then play a sound
	var h2 = document.getElementById('clock');

	/*function to calcutate the current time 
	then compare it to the alarmtime and play a sound when they are equal
	*/

	setInterval(function () {

		var date = new Date();

		var hours = date.getHours();
		// var hours = date.getHours();

		var minutes = date.getMinutes();

		var seconds = date.getSeconds();



		//convert military time to standard time

		var currentTime = h2.textContent = addZero(hours) + ":" + addZero(minutes) + ":" + addZero(seconds)


		if (alarmTime == currentTime) {
			sound.play();
		}

	}, 1000);


}


function alarmClear() {

	document.getElementById('alarmhrs').disabled = false;
	document.getElementById('alarmmins').disabled = false;
	document.getElementById('alarmsecs').disabled = false;
	sound.pause();
}